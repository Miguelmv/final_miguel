<?= $this->extend('layout/template') ?>

<?= $this->section('content') ?>
<?php $session = \Config\Services::session(); ?>

<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>

<?php $user = $auth->user()->row(); ?>

<?php if ($auth->loggedIn()): ?>
<div class="d-flex flex-row-reverse bd-highlight">
   
   <?= $user->first_name . ' ' . $user->last_name ?>
</div>

<div class="d-flex flex-row-reverse bd-highlight"><span>
       
        
            <a href="<?= site_url('auth/logout'); ?>">Desconectar</a>
            
            <?php else: ?>

          
        </span>
    
</div>
    <div class="p-2 bd-highlight"><span>
            <a  href="<?= site_url('auth/login') ?>">Entrar</a>

        </span></div>
<?php endif; ?>

<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <th>Código</th>
        <th>Producto</th>
        <th>Tallas</th>
        <th></th>
    </thead>
    
    <?php foreach ($productos as $producto): ?>
        <tr>
            <td><?= $producto['CodigoProducto'] ?></td>
            <td><?= $producto['Nombre'] ?></td>
            <td><?= $producto['Talla'] ?></td>
            
            <?php if ($auth->loggedIn() AND $auth->isAdmin()): ?>
            <td>
                <a href="<?= site_url('home/borrar/'.$producto['CodigoProducto'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar el producto<?= $producto['Nombre'] ?>')">Borrar</a>
            </td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>
