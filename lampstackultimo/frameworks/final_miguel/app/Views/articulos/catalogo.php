<?= $this->extend('layout/template') ?>

<?= $this->section('content') ?>

<?php $session = \Config\Services::session(); ?>

<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>

<?php $user = $auth->user()->row(); ?>

<div class="d-flex flex-row-reverse bd-highlight">
   
   <?= $user->first_name . ' ' . $user->last_name ?>
</div>

<?php if ($auth->loggedIn()): ?>
<div class="d-flex flex-row-reverse bd-highlight"><span>
       
        
            <a href="<?= site_url('auth/logout'); ?>">Desconectar</a>
            
            <?php else: ?>

          
        </span>
    
</div>
    <div class="p-2 bd-highlight"><span>
            <a  href="<?= site_url('auth/login') ?>">Entrar</a>

        </span></div>
<?php endif; ?>

<?php foreach ($catalogo as $categoria): ?>
<div><img src="<?= site_url('public/imagenes/iconostienda/0000001000_01.png')?>"><?= $categoria['NombreFamilia'] ?><br><?= $categoria['ProductosTotales'] ?></div>
<br><br>
<?php endforeach; ?>
