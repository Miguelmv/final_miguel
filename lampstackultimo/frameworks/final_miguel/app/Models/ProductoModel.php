<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of FamiliaModel
 *
 * @author a026640085j
 */
class ProductoModel extends Model{
    protected $table      = 'productos';
    protected $primaryKey = 'CodigoProducto';
    protected $returnType = 'array';
}
