<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of FamiliaModel
 *
 * @author a026640085j
 */
class FamiliaModel extends Model{
    protected $table      = 'familias';
    protected $primaryKey = 'id';
    protected $returnType = 'array';

}
