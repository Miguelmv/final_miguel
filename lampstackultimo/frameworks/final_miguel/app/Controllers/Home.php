<?php

namespace App\Controllers;

use App\Models\FamiliaModel;
use App\Models\ProductoModel;
use Config\Services;


class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index()
    {
        if (!$this->auth->loggedIn()){
            return redirect()->to('auth/login');
        }
        return view('welcome_message');
    }
    
    
        public function catalogo(){
        $familiaModel = new FamiliaModel();
        $data['titol'] = "Catalogo de Miguel";
        $data['catalogo'] = $familiaModel
                ->select("familias.CodigoFamilia, NombreFamilia, productos.CodigoProducto, count(productos.CodigoProducto) as 'ProductosTotales'")
                ->join('productos','productos.CodigoFamilia=familias.CodigoFamilia')
                ->findAll();
        return view('articulos/catalogo',$data);
    }
    
        public function productos(){
        $productoModel = new ProductoModel();
        $data['titol'] = "Catalogo productos de Miguel";
        $data['productos'] = $productoModel
                ->select("productos.CodigoProducto, productos.Nombre, productos.Talla")
                ->join('lineas','lineas.CodigoLinea=productos.CodigoLinea','LEFT')
                ->findAll();
        return view('articulos/productos',$data);
    }
    public function borrar($CodigoProducto){
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
        $productoModel = new ProductoModel();
        $productoModel->delete($CodigoProducto);
        return redirect()->to('home');
        } else {
            echo "No tienes los permisos suficientes para hacer esto.";
        }

    }


}
